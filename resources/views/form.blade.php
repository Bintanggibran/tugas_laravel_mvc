<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/register/welcome" method="GET">
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname"><br><br>

        <p>Gender:</p>

        <input type="radio" name="gender" id="male" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other">
        <label for="other">other</label><br>

        <label for="nationality">nationality: </label><br><br>
        <select id="nationality" name="nationality">
          <option value="indonesia">Indonesian</option>
          <option value="malaysian">malaysian</option>
          <option value="malaysian">australian</option>
        </select><br><br>

        <p>Language spoken:</p><br>
        <input type="checkbox" id="indonesia" name="indonesia" value="indonesia">
        <label for="vehicle1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="inggris" name="inggris" value="inggris">
        <label for="vehicle2"> English</label><br>
        <input type="checkbox" id="other" name="other" value="other">
        <label for="vehicle3"> other</label><br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="message" rows="10" cols="30"> </textarea><br>
        <input type="submit" value="Sign Up">  
    </form>
</body>
</html>